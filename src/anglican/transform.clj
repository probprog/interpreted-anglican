(ns anglican.transform
  (:refer-clojure :exclude [list*])
  (:require clojure.core)
  (:require [clojure.algo.monads
             :refer [with-monad domonad m-result m-map state-m]]))

;; list* does not do what it promises,
;; it should return a list rather than a sequence

(defn ^:private list*
  "Creates a new list containing the items prepended to the rest, the
  last of which will be treated as a sequence."
  [& args]
  (apply list (apply clojure.core/list* args)))

;; functions for traversing anglican programs and applying
;; source-to-source transformations

(defn ^:private transform-dispatch
  "dispatch function for transform,
  named for ease of debugging"
  [code tag tx]
  (if (:tags tx)
    (if (or (= (:tags tx) :all) ((:tags tx) tag))
      (:type tx)
      :default)
    [tag (:type tx)]))

(defmulti transform
  "transformer called by traverse,
  dispatches on [tag (:type tx)] or on (:type tx).
  Parameters:
  - code     the code to transform;
  - tag      the code tag;
  - tx       the transformation state,
             (:type tx) defines the transformation type;
             optionally, (:tags tx) is the set of tags
             to which the transformation is applied, in
             which case the dispatch is on the
             transformation type only. If (:tags tx) is :all,
             the transformation is applied to all tags.
  - return   accepts the transformed code and the
             updated transformation state."
  transform-dispatch)

;; the default transformation is to return the unmodified code
;; and the unchanged state

(defmethod transform :default [code _ tx] [code tx])

(declare ^:private traverse-expression)

(defn traverse
  "traverse anglican code tree,
  as produced by the parser. Parameters:
   - code     the source code
   - tx       the transformation to apply."
  [code tx]
  ((traverse-expression code) tx))

;; at many occasions the transformer applies transformations
;; through monadic computations to parts of an expression
;; and then calls transform on the re-built expression.
;; since transform is (almost) monadic itself, this leads
;; to a common pattern expressed in the `dotransform' macro.

(defmacro ^:private dotransform
  "macro wrapper over monadic steps,
  if the steps are supplied and are note empty,
  the final transform is performed on the result of the last
  step"
  ([code tag] `#(transform ~code ~tag %))
  ([steps code tag]
     (if (> (count steps) 0)
       `(domonad state-m [~@steps
                          txcode# #(transform ~code ~tag %)]
                 txcode#)
       `(dotransform code tag))))

;; The basic compound data structure in Scheme (and Anglican)
;; is a list. However, the clojure representation of the code
;; seems to be using various sequences interchangingly. Sticking
;; to always returning a proper list should be considered, as
;; a workaround the `a-list?' predicate is used.

(def ^:private a-pair?
  "true when the argument is an Anglican pair"
  #(and (seq? %) (seq %)))

(def ^:private a-nil?
  "true when the argument is Anglican nil"
  #(or (nil? %) (and (seq? %) (empty? %))))

(def ^:private a-list?
  "true when the argument is an Anglican list (a pair or nil)"
  #(or (a-pair? %) (a-nil? %)))

;; For uniformity, a-symbol? is defined.

(def ^:private a-symbol?
  "true when the argument is an Anglican symbol"
  symbol?)

(defn ^:private traverse-atom
  "traverses atom"
  [code]
  (dotransform code :atom))

(declare ^:private traverse-list)

(defn ^:private traverse-literal
  "traverses literal, either atom or list"
  [code]
  (if (a-list? code) (traverse-list code)   ; the anglican parser returns sequences
    (traverse-atom code)))                  ; rather than lists, should this be fixed?

(defn ^:private traverse-list
  "traverses list"
  [code]
  (dotransform
    [txlist (m-map traverse-literal code)]
    txlist :list))

(defn ^:private traverse-quote
  "traverses quote"
  [[quote literal]]
  (dotransform
    [txliteral (traverse-literal literal)]
    (list quote txliteral) :quote))

(declare ^:private traverse-expression)

(defn ^:private traverse-variable
   "traverses variable definition"
   [code]
   (dotransform code :variable))

(defn ^:private traverse-parameter
   "traverses parameter"
   [code]
   (dotransform code :parameter))

(defn ^:private traverse-parameter-list
  "traverses parameter list"
  [code]
  (cond
    (a-pair? code)
    (with-monad state-m
      (domonad
        [p (traverse-parameter (first code))
         ps (traverse-parameter-list (next code))]
        (cons p ps)))

    (a-nil? code)
    (with-monad state-m
      (m-result '()))

    :else
    (dotransform code :variadic)))

(defn ^:private traverse-parameters
    "traverse parameters"
    [code]
    (if (a-list? code) (traverse-parameter-list code)
        (dotransform code :variadic)))

(defn ^:private traverse-binding
  "traverses parameter"
  [[variable expression]]
  (dotransform
    [txvariable (traverse-variable variable)
     txexpression (traverse-expression expression)]
    (list txvariable txexpression) :binding))

 (defn ^:private traverse-reference
   "traverses variable reference"
   [code]
   (dotransform code :reference))

(defn ^:private traverse-application
  "traverses application"
  [code]
  (dotransform
    [txcode (m-map traverse-expression code)]
    txcode :application))

(defn ^:private traverse-lambda
  "traverses lambda form"
  [[lambda parameters & expressions]]
  (dotransform
    [txparameters (traverse-parameters parameters)
     txexpressions (m-map traverse-expression expressions)]
    (list* lambda txparameters txexpressions) :lambda))

(defn ^:private traverse-let
  "traverses let form"
  [[let bindings & expressions]]
  (dotransform
    [txbindings (m-map traverse-binding bindings)
     txexpressions (m-map traverse-expression expressions)]
    (list* let txbindings txexpressions) :let))

(defn ^:private traverse-define
  "traverses define form"
  [[define variable expression]]
  (dotransform
    [txvariable (traverse-variable variable)
     txexpression (traverse-expression expression)]
    (list define txvariable txexpression) :define))

(defn ^:private traverse-if
  "traverses if form"
  [[if cond then else]]
  (if (some? else)
    (dotransform
      [txcond (traverse-expression cond)
       txthen (traverse-expression then)
       txelse (traverse-expression else)]
      (list if txcond txthen txelse) :if)
    (dotransform
      [txcond (traverse-expression cond)
       txthen (traverse-expression then)]
      (list if txcond txthen) :if)))

(defn ^:private traverse-clause
  "traverses cond clause"
  [[test & expressions]]
  (let [m-txexpressions (with-monad state-m
                          (m-map traverse-expression expressions))]
    (if (= test 'else)
       (dotransform
         [txexpressions m-txexpressions]
         (list* test txexpressions) :cond-else)
       (dotransform
         [txtest (traverse-expression test)
          txexpressions m-txexpressions]
         (list* txtest txexpressions) :cond-clause))))

(defn ^:private traverse-cond
  "traverses cond form"
  [[cond & clauses]]
  (dotransform
    [txclauses (m-map traverse-clause clauses)]
    (list* cond txclauses) :cond))

(defn ^:private traverse-begin
  "traverses begin form"
  [[begin & expressions]]
  (dotransform
    [txexpressions (m-map traverse-expression expressions)]
    (list* begin txexpressions) :begin))

(defn ^:private traverse-observe
  "traverses observe form, the distribution fires the
  `:distribution' rather than `:application' event"
  [[observe distribution observation]]
  (dotransform
    [txdistribution (dotransform
                      [txexpressions (m-map traverse-expression distribution)]
                      txexpressions :distribution)
     txobservation (traverse-expression observation)]
    (list observe txdistribution txobservation) :observe))

(defn ^:private traverse-expression
  "traverses expression"
  [code]
  ((cond
    (a-pair? code) (case (first code)
                     quote   traverse-quote
                     lambda  traverse-lambda
                     let     traverse-let
                     define  traverse-define
                     if      traverse-if
                     cond    traverse-cond
                     begin   traverse-begin
                     observe traverse-observe
                     traverse-application)
    (a-symbol? code) traverse-reference
    :else traverse-atom)
   code))
