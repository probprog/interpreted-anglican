(ns anglican.main
  (:require [clojure.tools.cli :refer [cli]]
            [clojure.java.io :as io]
            [slingshot.slingshot :refer [try+ throw+]]
            [clojure.data.json :as json]
            [anglican.diag :refer [intercepting-errors]]
            [anglican.math :as math]
            [anglican.parser :as parser]
            [anglican.sampler :as sampler
             :refer [*predict* *predict-to-str*
                     *sample-count* *start-time* *map* nanotime]]
            [anglican.sampler.rdb :as rdb]
            [anglican.sampler.ardb :as ardb])
  (:require [anglican.sampler.pgibbs :as pgibbs]
            [anglican.sampler.smc :as smc]
            [anglican.sampler.cascade :as cascade])
  (:gen-class :main true))

(def ^:dynamic exit (fn [code] (System/exit code)))

(defn print-opts-and-args
  "Print out the options and the arguments"
  [opts args]
  (println (str "Options:\n" opts "\n\n"))
  (println (str "Arguments:\n" args "\n\n")))

(defn process-in-file-arg
  [filestr]
  (try+
   (cond
    (= filestr "*in*") *in*
    :default (io/reader filestr))
   (catch java.io.FileNotFoundException _
     (throw+ {:source :anglican
              :type :file-not-found
              :message (:message &throw-context)}))
   (catch java.net.UnknownHostException _
     (throw+ {:source :anglican
              :type :unknown-host
              :message (:message &throw-context)}))))

(defn process-out-file-arg
  [filestr]
  (cond
    (= filestr "*out*") *out*
    :default (io/writer filestr)))

(defmulti run (fn [code opts] (keyword (:sample-method opts))))

(defmacro binding-options
  "macro (binding-options option-map ns [:option-a :option-b ...] form ...)
  dynamically binds variables ns/*option-a*, ns/*option-b* ... to the values
  specified in the option-map for the keys, taking default from the existing
  bindings"
  [option-map ns options & body]
  `(binding ~(vec (mapcat (fn [option]
                            (let [varname (symbol (str ns) (str "*" (name option) "*"))]
                              [varname (list option option-map varname)]))
                          options))
     ~@body))

(defmethod run :rdb
  [code opts]
  (rdb/run code (:num-of-MH-per-sweep opts) (:num-samples opts)))

(defmethod run :ardb
  [code opts]
  (let [ardb-options (:ardb-options opts)]
    (binding-options ardb-options
                     ardb [:reward-concentration :backlog-length
                           :exploration-factor
                           :probability-estimate
                           :log-utility :reward-policy
                           :random-choice-projection :backlog-length
                           :training-sweeps
                           :print-adaptive-state :print-final-adaptive-state]
                     (ardb/run code (:num-of-MH-per-sweep opts) (:num-samples opts)))))

(defmethod run :smc
  [code opts]
  (let [run (if (:parallel opts) smc/prun smc/run)
        num-sweeps (or (:num-sweeps opts)
                       (Math/ceil (/ (:num-samples opts)
                                     (:num-particles opts))))
        num-particles (min (:num-particles opts) (:num-samples opts))]
    (binding [smc/*resample-threshold* (:resample-threshold opts)]
      (run code num-particles num-sweeps))))

(defmethod run :cascade
  [code opts]
  (let [; FIXME: we are currently abusing existing flags
        num-samples (:num-samples opts)
        num-threads (if (:parallel opts)
                      (+ (.availableProcessors (Runtime/getRuntime)) 2)
                      1)
        num-initial-particles (:num-particles opts)
        debug (:print-debug opts)]
      (cascade/run code
                   num-samples
                   num-threads
                   num-initial-particles
                   debug)))

(defmethod run :pgibbs
  [code opts]
  (let [run (if (:parallel opts) pgibbs/prun pgibbs/run)
        num-sweeps (or (:num-sweeps opts)
                       (Math/ceil (/ (:num-samples opts)
                                     (:num-particles opts))))
        num-particles (min (:num-particles opts) (:num-samples opts))]
    (binding [pgibbs/*retained-only* (:retained-particle-only opts)
              smc/*resample-threshold* (:resample-threshold opts)]
      (run code num-particles num-sweeps))))

(defn parse-args
  [args]
  (cli args
       ["-h" "--help" "Show help"
        :flag true
        :default false]
       ["-s" "--source-file" "Anglican source file to interpret"
        :default "*in*"]
       ["-p" "--predict-file" "File into which to print predicts"
        :default "*out*"]
       ["-m" "--sample-method" "Sampling method. Currently one of: smc, rdb, ardb, pgibbs, cascade"
        :default "pgibbs"
        :validate-fn #(#{"smc" "rdb" "ardb" "pgibbs" "cascade"} %)]
       ["-r" "--random-seed" "Seed for random number generator (integer)"
        :parse-fn #(Integer. %) :default nil]
       ["-c" "--print-sample-count" "Print sample count at every predict"
        :flag true :default false]
       ["-d" "--print-debug" "Print verbose debug info"
        :flag true :default false]
       ["-l" "--parallel" "Use parallel threads (particle methods only)"
        :flag true :default false]
       ["-ro" "--retained-particle-only" "Print predicts for all particles (pmcmc methods only)"
        :flag true :default false]
       ["-n" "--num-samples" "Number of samples"
        :parse-fn #(Integer. %) :default Double/POSITIVE_INFINITY]
       ["-P" "--num-particles" "Number of particles (must be greater than 1)"
        :parse-fn #(Integer. %) :default 100]
       ["-S" "--num-sweeps" "Number of sweeps"
        :parse-fn #(Integer. %) :default nil]
       ["-I" "--num-of-MH-per-sweep" "Number of MH iterations per sweep (for random DB)"
        :parse-fn #(Integer. %) :default 1]
       ["-R" "--resample-threshold" "Resample only when effective number of particles drops below R*P (SMC only)"
        :parse-fn #(Double. %) :default 1.0]
       ["--ardb-options" "--ardb-options" "A hash map of ardb (Adaptive RDB) options"
        :parse-fn #(read-string %)
        :default {}]
       ["-json" "--json-predicts" "Output predicts in JSON syntax"
        :flag true
        :default false]))

(defn -main
 "The Anglican interpreter main function that processes command line arguments"
 [& args]
 (let [[opts unparsed banner] (try+
                               (parse-args args)
                               (catch java.lang.Exception _
                                 (println (:message &throw-context))
                                 (println)
                                 (-main "-h")))]
   (intercepting-errors
     [:debug (:print-debug opts) :exit exit]
     (when (or (:help opts) (seq (filter #(not= % "main") unparsed)))
       (println banner)
       (exit 0))
     (if (:random-seed opts)
       (math/reset-rng! (:random-seed opts)))
     (let [code (parser/anglican-reader (process-in-file-arg (:source-file opts)))]
       (binding [*predict* (process-out-file-arg (:predict-file opts))
                 *predict-to-str* (if (:json-predicts opts)
                                    json/write-str
                                    sampler/to-str)
                 *sample-count* (if (:print-sample-count opts) 0 nil)
                 *start-time* (nanotime)]
         (run code opts)
         nil)))))
