(ns anglican.diag)

;;; Anglican diagnostics macros and functions.

(defmacro intercepting-errors
  "executes the body intercepting and properly reporting errors"
  [[& {:keys [debug exit] :or {debug false exit identity}}] & body]
  `(~'let [~'$debug ~debug
           ~'$exit ~exit]
     (~'try+
       ~@body
       ~@'((catch [:source :anglican] {:keys [type message]}
             (binding [*out* *err*]
               (println (str "["(name type)"] " message))
               (if $debug
                 (throw+)))
             ($exit 1))
           (catch java.lang.Exception _
             (binding [*out* *err*]
               (println "[error] " (:message &throw-context))
               (if $debug
                 (throw+)))
             ($exit 1))))))
