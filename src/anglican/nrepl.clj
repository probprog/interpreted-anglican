(ns anglican.nrepl
  (:require [slingshot.slingshot :refer [try+ throw+]]

            [clojure.pprint :refer [pprint]]
            [clojure.data.json :as json]

            [clojure.tools.nrepl.transport :as transport]
            [clojure.tools.nrepl.middleware.session :refer [session]]
            [clojure.tools.nrepl.middleware :refer [set-descriptor!]]
            [clojure.tools.nrepl.misc :refer [response-for]]

            [clojure.tools.nrepl :as nrepl] ; for the demo (see the bottom)

            [anglican.parser :as parser]
            [anglican.sampler :as sampler]
            [anglican.main :as main]))

;;; Block Queue for the predict queue
;; ([david] it should probably live in a separate module)

(declare empty-block-queue)
(def ^:dynamic *block-size* 100)
(deftype block-queue [blocks block]
  clojure.lang.IPersistentStack
  (pop [_] (if (seq blocks) (block-queue. (pop blocks) block)
               empty-block-queue))

  (peek [_] (if (seq blocks) (peek blocks) 
                block))

  (count [_] (+ (map count blocks) (count block)))

  (cons [_ item]  (if (= (count block) *block-size*)
                    (block-queue. (conj blocks block)
                                  (conj (empty block) item))
                    (block-queue. blocks
                                  (conj block item))))

  (empty [_] empty-block-queue)

  (equiv [_ other] (and (instance? block-queue other) 
                        (= blocks (:blocks other)) (= block (:block other))))

  (toString [this] (str (into (peek this) (if (seq blocks) ['...] [])))))

(def empty-block-queue
  (block-queue. clojure.lang.PersistentQueue/EMPTY []))

;; [david] defonce so that the namespace can be safely reloaded
;; without affecting currently running inference

(defonce ^:private directives (atom []))
(defonce ^:private inference (atom {:started false :thread nil}))
(def ^:private predict-queue (ref empty-block-queue))

(defn push-predict!
  "callback, called by the inference thread, adds a predict to the queue"
  [& values]

  ;; [david] inference is used as a flag to indicate that the inference
  ;; should be stopped. if the inference is nil, throw exception.
  ;; this will stop the orphaned thread.

  (when-not (:started @inference)
    (dosync
     (alter predict-queue empty)) ; we don't want the left-over predicts to pop-up on next inference
    (throw+ {:source :nrepl
             :type ::inference-stopped
             :message "inference stopped"}))
  (dosync
   (alter predict-queue conj values)))

;; [david] I believe that the name `flush-predicts!' is confusing.
;; The client does not flush them down the sink but rather retrieves them.

(defn retrieve-predicts
  "called by the client: retrieves generated predicts"
  []
  (dosync
   (let [predicts (peek @predict-queue)]
     (alter predict-queue pop)
     predicts)))

;; For compatibility, keep the old name
(def flush-predicts!  "deprecated alias for retrieve predict"
  retrieve-predicts)

(defn add-directives
  "called by the client: appends directives to the current anglican program"
  [text]
  (when (:started @inference)
    (throw+ {:source :nrepl
             :type ::invalid-command
             :message "Cannot add directives while inference is running"}))
  (let [code (parser/anglican-str text)]
    (swap! directives concat code)))

(defn clear-directives
  "called by the client: empties the current anglican program"
  []
  (when (:started @inference)
    (throw+ {:source :nrepl
             :type ::invalid-command
             :message "Cannot clear directives while inference is running"}))
  (reset! directives []))

(defn start-inference
  "starts inference"
  ([] (start-inference {}))
  ([opts]
     (when (:started @inference)
       (throw+ {:source :nrepl
                :type ::invalid-command
                :message "inference already started"}))

     ;; [david] if stop-inference is called, the thread will
     ;; through exception and thus exit on next predict.

     (let [new-thread (Thread.
                       #(binding [sampler/*output-predict* push-predict!]
                          (try+
                           
                           (main/run @directives
                             ;; use command-line defaults for unspecified options
                             (merge (first (main/parse-args [])) opts))
                           
                           (catch [:source :nrepl
                                   :type ::inference-stopped] {}
                             (swap! inference update-in [:thread] (fn [_] nil)))
                           
                           ;; Asynchronous exceptions are pushed into the predict
                           ;; queue, and will be available to the client when
                           ;; predicts are retrieved

                           (catch [] {:keys [source message]}
                             (push-predict! source message))
                           (catch Exception ex
                             (push-predict! :jvm (.getMessage ex))))))]
       (.start new-thread)
       (reset! inference {:started true :thread new-thread}))))

(defn stop-inference
  "called by the client: stops the inference"
  []
  (when-not (:started @inference)
    (throw+ {:source :nrepl
             :type ::invalid-command
             :message "inference not started"}))

  ;; [david] push predict checks whether the thread is not nil,
  ;; and will through exeption if it is nil, causing termination of the thread.
  ;; reminder - one should not call .stop on a thread in a normal java/clojure program
  ;; to avoid deadlocks.

  (swap! inference update-in [:started] (fn [_] false)))

(defn destroy-inference
  "called from the REPL on emergency: destroy the inference thread on emergency"
  []
  (when-not (:thread @inference)
    (throw+ {:source :nrepl
             :type ::invalid-command
             :message "No thread to destroy"}))
  (.stop (:thread @inference))
  (reset! inference {:started false :thread nil}))


;;; middleware

(defmacro just
  "macro to return nothing"
  [ & body]
  `(do  ~@body ~'(response-for msg :status :done)))

;; middleware should be specified in :repl-options :nrepl-middleware
;; in leiningen's project.clj. then NREPL can handle these messages
;; without the burden of eval.

(defn middleware
  "middleware wrapper"
  [handler]
  (fn [{:keys [op transport] :as msg}]
    (let [resp 
          (try+

           (condp = op
             "add-directives"    (just (add-directives (:directives msg)))
             "clear-directives"  (just (clear-directives))
             "start-inference"   (just (start-inference (read-string (str "{" (:options msg) "}"))))
             "stop-inference"    (just (stop-inference))
             "retrieve-predicts"
             (let [predicts (map (fn [[expr & rest]] (cons (str expr) (map json/write-str rest)))
                                 (retrieve-predicts))]
               (if (or (seq predicts) (:started @inference))
                 (response-for msg
                               :predicts predicts
                               :status :done)
                 (response-for msg
                               :status :done)))
             ;; otherwise
             nil)

           (catch [:source :nrepl] {error :message}
             (response-for msg
                           :error error
                           :status :done))
           (catch [:source :anglican] {type :type error :message}
             (response-for msg
                           :error (str (name type) ": " error)
                           :status :done))
           (catch Exception ex
             (response-for msg
                           :error (str "jvm error: " (.getMessage ex))
                           :status :done)))]
  (if (some? resp) (transport/send transport resp)
    (handler msg)))))

(set-descriptor! #'middleware
  {:requires #{#'session}
   :handles {"retrieve-predicts" {:doc "retrieves predicts"
                                  :requires {}
                                  :returns {"predicts" "a list of predicts"}}
             "add-directives"    {:doc "adds directives"
                                  :requires {"directives" "anglican source code"}}
             "clear-directives"  {:doc "clears directives"}
             "start-inference"   {:doc "starts inference"
                                  :requires {"options" "a sequence of keyword value options"}}
             "stop-inference"    {:doc "stops-inference"}}})

(defn nrepl-demo
  "connects to the server, and sends a sequence of requests"
  [& {:keys [port num-samples program sleep] :or {port 65432
                                                  num-samples 1
                                                  program "[predict 1]"
                                                  sleep 10}}]
  (with-open [conn (nrepl/connect :port port)]
    (let [client (nrepl/client conn 1000)
          print-predicts (fn [& {:keys [max] :or {max nil}}]
                           (let [resp (first (nrepl/message client {:op "retrieve-predicts"}))]
                             (doall resp)
                             (pprint (update-in resp [:predicts]
                                                #(format "<%d predicts here>" (count %))))
                             (doseq [predict ((if (some? max) #(take max %) identity)
                                              (:predicts resp))]
                               (pprint predict))))]

     (println "Loading an Anglican program")
      (-> client
          (nrepl/message {:op "add-directives"
                          :directives program})
          doall
          pprint)

      ;; start the inference

      (println "Starting the inference")
      (-> client
          (nrepl/message  {:op "start-inference"
                          :options (format ":sample-method \"pgibbs\" :num-samples %d" num-samples)})
          doall
          pprint)

      (print "Thinking ...")
      (Thread/sleep sleep)
      (println " should get some predicts by now")

      ;; Print some predicts

      (println "Some predicts while the inference is running")
      (print-predicts :max 10)

      (println "Stopping the inference")
      (-> client
          (nrepl/message {:op "stop-inference"})
          doall
          pprint)

      (println "Predicts after the inference stopped")
      (print-predicts :max 10)

      (println "Removing the program")
      (-> client
          (nrepl/message {:op "clear-directives"})
          doall
          pprint)

      (println "Done"))))
