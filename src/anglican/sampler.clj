(ns anglican.sampler
  (:refer-clojure :exclude [time])
  (:require [clojure.core :as core]
            [clojure.string :refer [join]]))

(declare to-str)

(def ^:dynamic *num-cpu* (.availableProcessors (Runtime/getRuntime)))
(def ^:dynamic *sample-count* nil)
(def ^:dynamic *start-time* nil)
(def ^:dynamic *predict* core/*out*)
(def ^:dynamic *predict-to-str* to-str)

(defn chunked-pmap
  ([f partition-size coll]
   (->> coll
        (partition-all partition-size)
        (pmap (comp doall
                    (partial map f)))
        (apply concat)))
  ([f partition-size coll & colls]
   (let [chunked-colls (apply (partial map vector)
                              (map (partial partition-all
                                            partition-size)
                                   (cons coll colls)))]
     (apply concat
            (pmap (partial apply
                           (comp doall
                                 (partial map f)))
                  chunked-colls)))))

(defn auto-pmap
  ([f coll]
   (let [partition-size (Math/ceil (/ (count coll) *num-cpu*))]
     (chunked-pmap f partition-size coll)))
  ([f coll & colls]
   (let [cs (cons coll colls)
         partition-size (Math/ceil (/ (reduce min (map count cs))
                                      *num-cpu*))]
     (apply chunked-pmap f partition-size coll colls))))

(def ^:dynamic *map* map)

(defprotocol Print
  (to-str [x]))

(extend-type nil
  Print
  (to-str [s] ""))

(extend-type Object
  Print
  (to-str [o] (pr-str o)))

(defn print-predict
  "Prints an expression expr along with its return value val"
  ([& predict]
   (binding [*out* *predict*]
      (println (join ","
                     (let [[expr & args] predict]
                       (cons (to-str expr) (map *predict-to-str* args))))))))

(defn nanotime
  []
  "Returns time in ns since *start-time*"
  (- (. System nanoTime) (or *start-time* 0)))

(defn time
  []
  "Returns time in ms since *start-time*"
  (/ (nanotime) 1.0e6))

(def ^:dynamic *output-predict* print-predict)
