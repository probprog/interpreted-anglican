(ns anglican.sampler.smc
  (:refer-clojure :exclude [eval apply])
  (:require [clojure.core :as clj]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [anglican.parser :as parser]
            [anglican.interpreter.base :as base
               :refer [Interpreter eval sub-eval apply observe dbg]]
            [anglican.math :as math
             :refer [log exp mean isfinite?]]
            [anglican.interpreter.smc :as smc
             :refer [log-weight]]
            [anglican.sampler
             :refer [*predict* *sample-count* *map* *output-predict*]]))

(declare sample-by-cdf run-assume run-observe run-predict)

(def ^:dynamic *resample-threshold* 1.0)
(def ^:dynamic *resample-method* math/sample-stratified)

(defprotocol ImportanceSampler
  (run-line [particles line])
  (resample [particles]))

(defrecord ParticleSet [states])

(defn new-samples?
  [states]
  (not (every? (partial = 0.0)
               (map smc/log-prop states))))

(defn need-resample? [log-w]
  (and
   ; weights must be > 0 and not NaN
   (some isfinite? log-w)
   ; the effective sample size must be below threshold
   (< (math/eff-sample-size log-w)
      (* *resample-threshold* (count log-w)))))

(defn resample* [particles]
  (let [; unnormalized log weights
        log-w (map log-weight (:states particles))
        ; number of particles
        L (count log-w)]
    (if (need-resample? log-w)
      ; resample particle set
      (let [as (*resample-method* (math/cdf-exp log-w) L)
            states (:states particles)
            new-log-w (* -1.0 (log (count states)))
            states (mapv (fn [a]
                           (assoc (get states a)
                             :log-weight new-log-w))
                         as)]
        (assoc particles
          :states states))
      ; return particle set without resampling
      particles)))

(defnp run-assume
  [particles line]
   "Runs an assume statement on a particle set, returning an
   updated set of states."
  (let [expr (:interpretation line)
        n (:line line)
        states (into [] (*map* (fn [s] (first (sub-eval s expr n)))
                               (:states particles)))]
    (assoc particles :states states)))

(defnp run-predict
  [particles line]
  "Runs a predict statement on a particle set, printing
  the return values to *predict*, and discarding any
  changes to the execution states."
  (let [expr (:interpretation line)
        n (:line line)
        state-val (*map* (fn [s] (sub-eval s expr n))
                         (:states particles))
        values (*map* (fn [[s v]] (if (isfinite? (:log-weight s)) v Double/NaN)) state-val)]
    (if (some? *sample-count*)
      (doseq [[l v] (map-indexed vector values)]
        (*output-predict* expr v (+ *sample-count* l)))
      (doseq [v values]
        (*output-predict* expr v)))
    particles))

(defnp run-observe
  [particles line]
  "Runs a observe statement on a particle set, printing
  the return values to *predict*, and discarding any
  changes to the execution states."
  (let [; :interpretation is of the form (erp-name arg arg arg ... value)
        ; TODO this is retarded and we should fix it
        expr (nth (:interpretation line) 1)
        value  (nth (:interpretation line) 2)
        n (:line line)
        states (into []
                (*map* (fn [s]
                         (-> s
                             (base/enter-expr n)
                             (observe expr value)
                             first
                             (base/exit-expr)))
                       (:states particles)))]
      ; preform resampling (if necessary)
      (resample (assoc particles :states states))))

(def run-methods
  {:assume run-assume
   :observe run-observe
   :predict run-predict})

(defn run-line* [particles line]
  (let [run-fn (run-methods (keyword (:command line)))]
    (run-fn particles line)))

(def sampler-methods
  {:run-line run-line*
   :resample resample*})

(extend ParticleSet
  ImportanceSampler
  sampler-methods)

(defn run
  "Runs SMC on code, using num-part particles."
  ([code num-part]
   (loop [particles (ParticleSet. (into [] (repeat num-part (base/reset-time smc/empty-state))))
          lines code]
     (if (empty? lines)
       particles
       (recur (run-line particles (first lines))
              (rest lines)))))
  ([code num-part num-sweep]
   (loop [; index of upcoming sweep
          n 1]
     (let [particles (run code num-part)]
       ; increment (completed) sample count
       (if (some? *sample-count*)
         (set! *sample-count* (* n num-part)))
       ; return or continue to next sweep
       (if (>= n num-sweep)
         particles
         (recur (inc n)))))))

(defn prun [& args]
  "Same as run, but uses pmap instead of map to execute statements in parallel"
  (binding [*map* clj/pmap]
    (clj/apply run args)))
