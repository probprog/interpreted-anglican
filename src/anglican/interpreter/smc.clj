(ns anglican.interpreter.smc
  (:refer-clojure :exclude [eval apply])
  (:require [slingshot.slingshot :as slingshot :refer [throw+ try+]]
            [anglican.math :as math :refer [log exp log-sum-exp]]
            [anglican.interpreter.base
             :as base
             :refer [Interpreter eval apply observe]])
  (:import [anglican.interpreter.base
              Sample ElementaryRandomProcedure ExchangeableRandomProcedure]))

; A SMC execution state contains
; {:log-weight  double
;  :log-prop    double
;  :proc-state  {addr {} ...}
;  :env-stack   [EnvironmentFrame ...]
;  :eval-addr  addr
;  :system {}}
(defrecord ExecutionState [log-weight log-prop proc-state env-stack eval-addr system])

(defn add-log-weight [state s]
  (update-in state
             [:log-weight]
             (fn [value]
               (+ (or value 0.0)
                  (:log-prob s)))))

(defn set-log-weight [state value]
  (assoc state :log-weight value))

(defn add-log-prop [state s]
  (update-in state
             [:log-prop]
             (fn [value]
               (+ (or value 0.0)
                  (:log-prob s)))))

(defn set-log-prop [state value]
  (assoc state :log-prop value))

(defn weight [state]
  (let [lw (:log-weight state)]
    (if (some? lw)
      (exp lw)
      0.0)))

(defn log-weight [state]
  (or (:log-weight state) 0.0))

(defn log-prop [state]
  (or (:log-prop state) 0.0))

(defn observe*
  "Evaluates an random expression with constrained return value.
  Returns a Sample object."
  [state expr value]
  (let [[state s] (base/observe* state expr value)]
    [(add-log-weight state s) s]))

(defn sample-erp
  [state proc args]
  (let [[state sample] (base/sample-erp state proc args)]
    [(add-log-prop state sample) sample]))

(defn sample-xrp
  [state proc args]
  (let [[state sample] (base/sample-xrp state proc args)]
    [(add-log-prop state sample) sample]))

(def sample-methods
  (assoc base/sample-methods
         ElementaryRandomProcedure sample-erp
         ExchangeableRandomProcedure sample-xrp))

(defn sample*
  [state proc args]
  (let [state (base/inc-count state :calls :sample)
        sample-fn (get sample-methods (class proc) base/sample-invalid)]
   (sample-fn state proc args)))

(def intepreter-methods
  (assoc base/intepreter-methods
         :sample sample*
         :observe observe*))

(extend ExecutionState
  Interpreter
  intepreter-methods)

(def empty-state
  (ExecutionState.
   0.0
   0.0
   {}
   [base/global-env]
   [:global]
   {}))
