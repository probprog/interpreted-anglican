(ns anglican.stochastic-tests
  (:require [clojure.core.reducers :as r]
            [clojure.set :refer [union]]
            [clojure.string :as string]
            [incanter.stats :refer [chisq-test]]
            [slingshot.slingshot :refer [try+ throw+]]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.java.io :as io]
            [anglican.parser :as parser]
            [anglican.math :as math]
            [anglican.math.erp :as erp]
            [anglican.main]
            [anglican.sampler :refer [*predict* *map* *sample-count* *output-predict*]]
            [anglican.sampler.rdb :as rdb]
            :reload)
  (:use clojure.test))

(defn run-source [file-name run-options]
  "Returns a coll of predicts given a file-name of the Anglican source and run-options"
  (let
    [predict-queue (ref [])]
    (binding [anglican.sampler/*output-predict*
                (fn [predict-expr predict-result & rest-things]
                  (dosync (alter predict-queue conj predict-result)))]
      (anglican.main/run
        (parser/anglican-file file-name)
        (merge (first (anglican.main/parse-args []))
           run-options))
      @predict-queue)))

(defn run-ks-test [file-name options cdf confidence num-fail]
  (loop [remaining num-fail]
    (if (= remaining 0)
      ; all consequtive trials failed -> fail test
      false
      ; run trial
      (let [; run sampler
            samples (run-source file-name options)
            ; get effective sample size
            counts (vals (frequencies samples))
            ess (math/eff-sample-size (map math/log counts))
            Q (math/ks-quantile samples cdf ess)]
        (if (< Q confidence)
          ; at least one trial passed -> pass test
          true
          ; continue with next trial
          (do
            (recur (dec remaining))))))))

(defn run-g-test [file-name options lnpdf confidence num-fail]
  (loop [remaining num-fail]
    (if (= remaining 0)
      ; all consequtive trials failed -> fail test
      false
      ; run trial
      (let [samples (run-source file-name options)
            Q (math/g-quantile samples lnpdf)]
        (if (< Q confidence)
          ; at least one trial passed -> pass test
          true
          ; continue with next trial
          (do
            (recur (dec remaining))))))))

(def run-options
  [{:sample-method :pgibbs
    :num-particles 100
    :num-sweeps 10}
   {:sample-method :smc
    :num-particles 1000
    :num-sweeps 1}
   {:sample-method :rdb
    :num-of-MH-per-sweep 10
    :num-samples 1000}])

(def cont-erp-tests
  [{:file "resources/stochastic-tests/erp-simple/beta-simple.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.BetaDist. 1.0 2.0) x))}
   {:file "resources/stochastic-tests/erp-simple/exponential-simple.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.ExponentialDist. 1.0) x))}
   {:file "resources/stochastic-tests/erp-simple/gamma-simple.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.GammaDist. 1.0 2.0) x))}
   {:file "resources/stochastic-tests/erp-simple/normal-simple.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.NormalDist. 5.0 1.0) x))}
   {:file "resources/stochastic-tests/erp-simple/uniform-continuous-simple.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.UniformDist. 2.4 5.0) x))}])


(def disc-erp-tests
  [{:file "resources/stochastic-tests/erp-simple/binomial-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.BinomialDist. 10 0.77) x)))}
   {:file "resources/stochastic-tests/erp-simple/categorical-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.DiscreteDistribution.
                                     (int-array (list (int 0) (int 1) (int 2) (int 3) (int 4)))
                                     (double-array (list (double 0.1) (double 0.2) (double 0.3) (double 0.3) (double 0.1)))
                                     (int 5))
                                    x)))}
   {:file "resources/stochastic-tests/erp-simple/discrete-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.DiscreteDistribution.
                                      (int-array (list (int 0) (int 1) (int 2) (int 3) (int 4)))
                                      (double-array (list (double 0.1) (double 0.2) (double 0.3) (double 0.3) (double 0.1)))
                                      (int 5))
                                    x)))}
   {:file "resources/stochastic-tests/erp-simple/bernoulli-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.BernoulliDist. 0.77) (if x 1 0))))}
   {:file "resources/stochastic-tests/erp-simple/poisson-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.PoissonDist. 4.7) x)))}
   {:file "resources/stochastic-tests/erp-simple/uniform-discrete-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.UniformIntDist. 0 4) x)))}])

(def cond-erp-tests
  [{:file "resources/stochastic-tests/erp-conditional/bernoulli-conditional.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.BetaDist. 4.0 3.0) x))}
   {:file "resources/stochastic-tests/erp-conditional/binomial-conditional.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.BetaDist. 17.0 26.0) x))}
   {:file "resources/stochastic-tests/erp-conditional/normal-unknown-mean.anglican"
    :cdf (fn [x] (.cdf (umontreal.iro.lecuyer.probdist.NormalDist. 6.0 0.4472135955) x))}])

(def disc-xrp-tests
  [{:file "resources/stochastic-tests/xrp-simple/flip-beta-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.BernoulliDist. (/ 4 7)) (if x 1 0))))}
   {:file "resources/stochastic-tests/xrp-simple/crp-simple.anglican"
    :lnpdf (fn [x] (math/log (.prob (umontreal.iro.lecuyer.probdist.DiscreteDistribution.
                                     (int-array (list (int 1) (int 2) (int 3) (int 4)))
                                     (double-array (list (double 0.2) (double 0.4) (double 0.2) (double 0.2)))
                                     (int 4))
                                    (dec x))))}])

(deftest erp-continuous-simple-tests
  (doseq [erp-test cont-erp-tests
          run-opts run-options]
    (testing (str (:file erp-test) "," run-opts)
      (let [passes (run-ks-test (:file erp-test) run-opts (:cdf erp-test) 0.9 3)]
        (is passes)))))

(deftest erp-discrete-simple-tests
  (doseq [erp-test disc-erp-tests
          run-opts run-options]
    (testing (str (:file erp-test) "," run-opts)
      (let [passes (run-g-test (:file erp-test) run-opts (:lnpdf erp-test) 0.95 2)]
        (is passes)))))

(deftest erp-conditional-tests
  (doseq [erp-test cond-erp-tests
          run-opts run-options]
    (testing (str (:file erp-test) "," run-opts)
      (let [passes (run-ks-test (:file erp-test) run-opts (:cdf erp-test) 0.90 3)]
        (do
          (is passes))))))

(deftest xrp-discrete-simple-tests
  (doseq [xrp-test disc-xrp-tests
          run-opts run-options]
    (testing (str (:file xrp-test) "," run-opts)
      (let [passes (run-g-test (:file xrp-test) run-opts (:lnpdf xrp-test) 0.95 2)]
        (is passes)))))
