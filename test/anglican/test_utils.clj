(ns anglican.test-utils)

;;; Test Utilities: Functions to facilitate automated testing of Anglican.
;; placed in the test rather than source subtree, but should not contain
;; real tests.

(defn parse-anglican-source
  [source])

(defn parse-anglican-output
  "parse anglican output"
  [output])

(defn run-code
  "parse code and return parsed output"
  [code options])

