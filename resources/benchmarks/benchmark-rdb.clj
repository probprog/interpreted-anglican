(ns benchmark-rdb
  (:require [clojure.core.reducers :as r]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.java.io :as io]
            [anglican.parser :as parser]
            [anglican.math :as math]
            [anglican.sampler :refer [*predict* *map* *sample-count* *start-time* nanotime]]
            [anglican.sampler.rdb :as rdb] :reload))

(defn benchmark-rdb [src-file MH-per-sweep sweeps seed]
  (let [code (parser/venture-file src-file)]
    (math/reset-rng! seed)
    (rdb/run code MH-per-sweep sweeps)))

(binding [*predict* (io/writer "benchmark-rdb-hwkot.out")
          *out* (io/writer "benchmark-rdb-hwkot.profile")
          *sample-count* 0
          *start-time* (nanotime)
          *map* pmap]
  (profile :info :Arithmetic
           (benchmark-rdb
            "resources/test-anglican-code/hmm-with-known-obs-and-trans.venture"
            10 10 1)))
