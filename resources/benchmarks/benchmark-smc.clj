(ns benchmark-pgibbs
  (:require [clojure.core.reducers :as r]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.java.io :as io]
            [anglican.parser :as parser]
            [anglican.math :as math]
            [anglican.sampler :refer [*predict* *map* *sample-count* *start-time* nanotime]]
            [anglican.sampler.smc :as smc] :reload-all))

(defn benchmark-smc [src-file num-part num-sweep seed]
  (let [code (parser/venture-file src-file)]
    (math/reset-rng! seed)
    (smc/run code num-part num-sweep)))

 (binding [*predict* (io/writer "benchmark-smc-hwkot-map.out")
           *out* (io/writer "benchmark-smc-hwkot-map.profile")
           *sample-count* 0
           *start-time* (nanotime)
           *map* map]
  (profile :info :Arithmetic
    (benchmark-smc
      "resources/test-anglican-code/hmm-with-known-obs-and-trans.venture"
      10 10 1))
  nil)
