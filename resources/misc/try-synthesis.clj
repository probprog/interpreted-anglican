(ns try-synthesis
  (:require [clojure.core.reducers :as r]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.java.io :as io]
            [anglican.parser :as parser]
            [anglican.math :as math]
            [anglican.interpreter.base :as base]
            [anglican.interpreter.rdb]
            [anglican.sampler :refer [*predict* *map* *sample-count*]]
            [anglican.sampler.rdb]))

(defn run
  "Runs MH on code."
  ([code burn-in-iterations num-MH-iterations-per-sweep num-sweeps]
    (let
      [empty-state (base/reset-time anglican.interpreter.rdb/empty-state)
       prior-state (anglican.sampler.rdb/run-code code empty-state)
       state-after-burn-in (anglican.sampler.rdb/run-MH code prior-state (dec burn-in-iterations))]
    (loop [state state-after-burn-in
           remain-sweeps num-sweeps
           predicts (vector)]
      (if (> remain-sweeps 0)
        (let
          [new-state
             (try
               (anglican.sampler.rdb/run-MH code state num-MH-iterations-per-sweep)
               (catch Exception e state))]
          (recur
            new-state
            (dec remain-sweeps)
            (print (str (second (first (vals (get-in state [:predicts])))) "\n"))))
        predicts)))))

(defn run-synthesis [src-file burn-in MH-per-sweep sweeps seed]
  (let [code (parser/venture-file src-file)]
    (math/reset-rng! seed)
    (run code burn-in MH-per-sweep sweeps)))

(run-synthesis "C:/automatic_programming_1/new_1.lsp" 1 10 200 1)
